package com.q_rim.app8_2;

import android.support.v4.app.Fragment;

public class RunActivity extends SingleFragmentActivity {

  @Override
  protected Fragment createFragment() {
    return new RunFragment();
  }
}
