package com.q_rim.app8_2;

import android.app.PendingIntent;
import android.app.admin.SystemUpdatePolicy;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;

public class RunManager_Singleton {
  private static final String TAG = "Q---: RunManager_Singleton";

  public static final String ACTION_LOCATION = "com.q_rim.app8_2.ACTION_LOCATION";

  private static RunManager_Singleton runManager;
  private Context appContext;
  private LocationManager locationManager;

  // The private constructor forces users to use RunManager.get(context)
  private RunManager_Singleton(Context appContext) {
    this.appContext = appContext;
    this.locationManager = (LocationManager)this.appContext
      .getSystemService(Context.LOCATION_SERVICE);
  }

  public static RunManager_Singleton get(Context c) {
    if (runManager == null) {
      // Use the application context to avoid leaking activities
      runManager = new RunManager_Singleton(c.getApplicationContext());
    }
    return runManager;
  }

  private PendingIntent getLocationPendingIntenet(boolean shouldCreate) {
    Intent broadcast = new Intent(ACTION_LOCATION);
    int flags = shouldCreate ? 0 : PendingIntent.FLAG_NO_CREATE;
    return PendingIntent.getBroadcast(this.appContext, 0, broadcast, flags);
  }

  public void startLocationUpdates() {
    String provider = LocationManager.GPS_PROVIDER;

    // Get the last known location and broadcast it if you have one
    Location lastKnown = this.locationManager.getLastKnownLocation(provider);
    if (lastKnown != null) {
      // Reset the time to now
      lastKnown.setTime(System.currentTimeMillis());
      broadcastLocation(lastKnown);
    }

    // Start updates from the location manager
    PendingIntent pi = getLocationPendingIntenet(true);
    this.locationManager.requestLocationUpdates(provider, 0, 0, pi);
  }

  private void broadcastLocation(Location location) {
    Intent broadcast = new Intent(ACTION_LOCATION);
    broadcast.putExtra(LocationManager.KEY_LOCATION_CHANGED, location);
    this.appContext.sendBroadcast(broadcast);
  }

  public void stopLocationUpdates() {
    PendingIntent pi = getLocationPendingIntenet(false);
    if (pi != null) {
      this.locationManager.removeUpdates(pi);
      pi.cancel();
    }
  }

  public boolean isTrackingRun() {
    return getLocationPendingIntenet(false) != null;
  }
}
