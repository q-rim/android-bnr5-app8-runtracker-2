package com.q_rim.app8_2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class RunFragment extends Fragment{

  private BroadcastReceiver locationReceiver = new LocationReceiver() {

    @Override
    protected void onLocationReceived(Context context, Location loc) {
      lastLocation = loc;
      if (isVisible())
        updateUI();
    }

    @Override
    protected void onProviderEnabledChanged(boolean enabled) {
      int toastText = enabled ? R.string.gps_enabled : R.string.gps_disabled;
      Toast.makeText(getActivity(), toastText, Toast.LENGTH_SHORT).show();
    }
  };

  private RunManager_Singleton runManager;
  private Run run;
  private Location lastLocation;

  private Button startButton, stopButton;
  private TextView started_TextView, latitude_TextView, longitude_TextView, altitude_TextView, duration_TextView;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setRetainInstance(true);
    this.runManager = RunManager_Singleton.get(getActivity());
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_run, container, false);

    this.started_TextView = (TextView)view.findViewById(R.id.run_startedTextView);
    this.latitude_TextView = (TextView)view.findViewById(R.id.run_latitudeTextView);
    this.longitude_TextView = (TextView)view.findViewById(R.id.run_longitudeTextView);
    this.altitude_TextView = (TextView)view.findViewById(R.id.run_altitudeTextView);
    this.duration_TextView = (TextView)view.findViewById(R.id.run_durationTextView);

    this.startButton = (Button)view.findViewById(R.id.run_startButton);
    this.startButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        runManager.startLocationUpdates();
        run = new Run();
        updateUI();
      }
    });

    this.stopButton = (Button)view.findViewById(R.id.run_stopButton);
    this.stopButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        runManager.stopLocationUpdates();
        updateUI();
      }
    });


    updateUI();
    return view;
  }

  @Override
  public void onStart() {
    super.onStart();
    getActivity().registerReceiver(locationReceiver, new IntentFilter(RunManager_Singleton.ACTION_LOCATION));
  }

  @Override
  public void onStop() {
    getActivity().unregisterReceiver(locationReceiver);
    super.onStop();
  }

  private void updateUI() {
    boolean started = this.runManager.isTrackingRun();

    if (run != null)
      started_TextView.setText(run.getStartDate().toString());

    int duruationSeconds = 0;
    if (run != null && lastLocation != null) {
      duruationSeconds = run.getDurationsSeconds(lastLocation.getTime());
      latitude_TextView.setText(Double.toString(lastLocation.getLatitude()));       // TextView - set latitutde
      longitude_TextView.setText(Double.toString(lastLocation.getLongitude()));     // TextView - set longitude
      altitude_TextView.setText(Double.toString(lastLocation.getAltitude()));       // TextView - set altitude
    }
    duration_TextView.setText(Run.formatDuration(duruationSeconds));                // TextView - set duration time

    this.startButton.setEnabled(!started);
    this.stopButton.setEnabled(started);
  }
}
